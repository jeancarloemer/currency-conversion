import React from "react";

import Router from "next/router";
import { GetServerSideProps } from "next";

import {
  fetchExchangeRate,
  fetchHistoricalRates,
} from "../utils/alpha-vantage";

import Layout from "../components/Layout";
import ConversionForm from "../components/ConversionForm";
import ConvertedAmount from "../components/ConvertedAmount";
import { HistoricalRatesChart } from "../components/HistoricalRatesChart";

interface Props {
  exchangeRate: number | null;
  exchangeRateErr: string | null;
  historicalRates: Array<{ date: string; rate: number }> | null;
  historicalRatesErr: string | null;
  amount: number;
  from: string;
  to: string;
}

const IndexPage = ({
  amount,
  from,
  to,
  exchangeRate,
  exchangeRateErr,
  historicalRates,
  historicalRatesErr,
}: Props) => {
  const [values, setValues] = React.useState({ amount, from, to });

  React.useEffect(() => {
    setValues({ amount, from, to });
  }, [amount, from, to]);

  return (
    <Layout title="Currency Conversion">
      <ConversionForm
        onChangeAmount={(amount) => {
          const newValues = { ...values, amount };
          setValues(newValues);
          Router.replace({ pathname: "/", query: newValues }, undefined, {
            shallow: true,
          });
        }}
        onChangeFrom={(from) => {
          const newValues = { ...values, from };
          setValues(newValues);
          Router.push({ pathname: "/", query: newValues });
        }}
        onChangeTo={(to) => {
          const newValues = { ...values, to };
          setValues(newValues);
          Router.push({ pathname: "/", query: newValues });
        }}
        {...values}
      />
      <ConvertedAmount
        exchangeRate={exchangeRate}
        exchangeRateErr={exchangeRateErr}
        amount={values.amount}
        from={from}
        to={to}
      />
      <HistoricalRatesChart
        historicalRatesErr={historicalRatesErr}
        historicalRates={historicalRates}
      />
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({
  query,
}): Promise<{ props: Props }> => {
  const amount = Number(query.amount) || 0;
  const from = (query.from as string) || "USD";
  const to = (query.to as string) || "EUR";

  let exchangeRateErr = null;
  let historicalRatesErr = null;

  const exchangeRateRequest = fetchExchangeRate({ from, to }).catch((err) => {
    console.error((err as Error).message);
    // Max API call reached or unexpected currency pair
    exchangeRateErr = "Error performing the conversion. Try again.";
    return null;
  });

  const historicalRatesRequest = fetchHistoricalRates({ from, to }).catch(
    (err) => {
      console.error((err as Error).message);
      // Some currency pairs doesn't have datas
      historicalRatesErr = "Historical data not available.";
      return null;
    }
  );

  const exchangeRate = await exchangeRateRequest;
  const historicalRates = await historicalRatesRequest;

  return {
    props: {
      amount,
      exchangeRate,
      exchangeRateErr,
      historicalRates,
      historicalRatesErr: !exchangeRateErr ? historicalRatesErr : null,
      from,
      to,
    },
  };
};

export default IndexPage;
