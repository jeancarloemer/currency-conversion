export function numberToString(value: number, currency: string) {
  var formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency,
    minimumFractionDigits: 2,
  });
  return formatter.format(value);
}

export function currencyPrefix(currency: string) {
  return numberToString(0, currency).split("0")[0];
}
