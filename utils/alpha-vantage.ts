import fetch from "node-fetch";
import { stringify } from "query-string";

interface Currencies {
  from: string;
  to: string;
}

function buildURL(params: Record<string, string>) {
  return `https://www.alphavantage.co/query?${stringify({
    apikey: process.env.ALPHA_VANTAGE_API_KEY,
    ...params,
  })}`;
}

export async function fetchExchangeRate({ from, to }: Currencies) {
  const response = await fetch(
    buildURL({
      function: "CURRENCY_EXCHANGE_RATE",
      from_currency: from,
      to_currency: to,
    })
  );

  const result = await response.json();
  try {
    return Number(
      result["Realtime Currency Exchange Rate"]["5. Exchange Rate"]
    );
  } catch (error) {
    throw new Error(
      `fetchExchangeRate unexpected result: ${JSON.stringify(result)}`
    );
  }
}

export async function fetchHistoricalRates({ from, to }: Currencies) {
  const response = await fetch(
    buildURL({
      function: "FX_DAILY",
      from_symbol: from,
      to_symbol: to,
    })
  );

  const result = await response.json();
  try {
    return Object.entries(result["Time Series FX (Daily)"])
      .map(([key, value]) => ({
        date: key,
        rate: Number((value as any)["4. close"]),
      }))
      .slice(0, 30);
  } catch (error) {
    throw new Error(
      `fetchHistoricalRates unexpected result: ${JSON.stringify(result)}`
    );
  }
}
