import React from "react";

import MenuItem from "@material-ui/core/MenuItem";
import TextField, { TextFieldProps } from "@material-ui/core/TextField";
import { styled } from "@material-ui/core";

import { currencies } from "../utils/currencies";

const CurrencyCode = styled("strong")({ marginRight: "6px" });

export function CurrencySelector(props: TextFieldProps) {
  return (
    <TextField {...props} variant="outlined" select>
      {currencies.map(({ code, name }) => (
        <MenuItem key={code} value={code}>
          <CurrencyCode>{code}</CurrencyCode>
          {name}
        </MenuItem>
      ))}
    </TextField>
  );
}
