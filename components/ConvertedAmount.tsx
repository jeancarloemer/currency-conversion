import { styled, Typography } from "@material-ui/core";
import { Alert } from "@material-ui/lab";

import { numberToString } from "../utils/formatters";

interface Props {
  exchangeRate: number | null;
  exchangeRateErr: string | null;
  amount: number;
  from: string;
  to: string;
}

const Wrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(2),
}));

export default function ConvertedAmount({
  exchangeRate,
  exchangeRateErr,
  amount,
  from,
  to,
}: Props) {
  return (
    <Wrapper>
      {exchangeRate && (
        <>
          <Typography variant="h1">
            {amount ? numberToString(exchangeRate * amount, to) : "\u00A0"}
          </Typography>
          <Typography variant="h5">
            1 {from} = <strong>{exchangeRate}</strong> {to}
          </Typography>
        </>
      )}
      {exchangeRateErr && <Alert severity="error">{exchangeRateErr}</Alert>}
    </Wrapper>
  );
}
