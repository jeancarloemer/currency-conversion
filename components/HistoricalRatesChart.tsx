import { ResponsiveLine } from "@nivo/line";
import { Paper, Typography, styled, useTheme } from "@material-ui/core";
import { timeFormat } from "d3-time-format";
import { Alert } from "@material-ui/lab";

interface Props {
  historicalRates: Array<{ date: string; rate: number }> | null;
  historicalRatesErr: string | null;
}

const DATE_FORMAT = "%b %d";

function formatData(historicalRates: Array<{ date: string; rate: number }>) {
  return [
    {
      id: "Rates",
      data: historicalRates.map(({ date, rate }) => ({
        x: new Date(date),
        y: rate,
      })),
    },
  ];
}

const Tooltip = styled(Paper)(({ theme }) => ({
  display: "flex",
  alignItems: "baseline",
  padding: theme.spacing(1, 2),
}));

const Wrapper = styled("div")({
  minHeight: 400,
  maxWidth: 900,
  height: "40vh",
});

export function HistoricalRatesChart({
  historicalRates,
  historicalRatesErr,
}: Props) {
  const theme = useTheme();

  return (
    <Wrapper>
      {historicalRates && (
        <ResponsiveLine
          data={formatData(historicalRates)}
          margin={{ top: 50, right: 50, bottom: 50, left: 60 }}
          curve="linear"
          colors={[theme.palette.primary.light]}
          xScale={{
            type: "time",
            precision: "day",
          }}
          yScale={{
            type: "linear",
            stacked: false,
            min: "auto",
            max: "auto",
          }}
          axisBottom={{
            format: DATE_FORMAT,
          }}
          axisLeft={{
            orient: "left",
            tickSize: 5,
            tickPadding: 5,
          }}
          pointSize={theme.spacing(1)}
          pointBorderWidth={2}
          enableGridX={false}
          enableSlices="x"
          sliceTooltip={({ slice }) => (
            <Tooltip>
              <Typography variant="h6" component="p">
                {slice.points[0].data.yFormatted}
              </Typography>
              {"\u00A0"}
              {timeFormat(DATE_FORMAT)(slice.points[0].data.x as Date)}
            </Tooltip>
          )}
        />
      )}
      {historicalRatesErr && (
        <Alert severity="error">{historicalRatesErr}</Alert>
      )}
    </Wrapper>
  );
}
