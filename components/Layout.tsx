import React, { ReactNode } from "react";

import Head from "next/head";
import { styled, Paper } from "@material-ui/core";

type Props = {
  children?: ReactNode;
  title?: string;
};

const Overlay = styled("div")(({ theme }) => ({
  display: "flex",
  minHeight: "100vh",
  background: theme.palette.text.secondary,
}));

const Container = styled(Paper)(({ theme }) => ({
  margin: "auto",
  width: "100vw",
  maxWidth: 900 + theme.spacing(6),
  padding: theme.spacing(5, 3),
}));

const Layout = ({ children, title = "This is the default title" }: Props) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Overlay>
      <Container>{children}</Container>
    </Overlay>
  </div>
);

export default Layout;
