import React from "react";

import { makeStyles, styled } from "@material-ui/core/styles";
import { CurrencySelector } from "./CurrencySelector";
import { AmountInput } from "./AmountInput";

const Wrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(1),
}));

const Row = styled("div")({
  display: "flex",
  maxWidth: 600,
});

const useStyles = makeStyles((theme) => ({
  amountField: {
    flexGrow: 1,
    margin: theme.spacing(1),
  },
  currencyField: {
    flexGrow: 1,
    flexBasis: 0,
    margin: theme.spacing(1),
  },
}));

interface Props {
  from: string;
  to: string;
  amount: number;
  onChangeFrom: (value: string) => void;
  onChangeTo: (value: string) => void;
  onChangeAmount: (value: number) => void;
}

export default function ConversionForm({
  from,
  to,
  amount,
  onChangeAmount,
  onChangeFrom,
  onChangeTo,
}: Props) {
  const classes = useStyles();

  return (
    <Wrapper>
      <Row>
        <AmountInput
          className={classes.amountField}
          currency={from}
          amount={amount}
          onChangeAmount={onChangeAmount}
          label="Amount"
        />
      </Row>
      <Row>
        <CurrencySelector
          className={classes.currencyField}
          value={from}
          onChange={({ target: { value } }) => onChangeFrom(value)}
          label="From"
        />
        <CurrencySelector
          className={classes.currencyField}
          value={to}
          onChange={({ target: { value } }) => onChangeTo(value)}
          label="To"
        />
      </Row>
    </Wrapper>
  );
}
