import React from "react";

import NumberFormat from "react-number-format";
import TextField, { TextFieldProps } from "@material-ui/core/TextField";

import { currencyPrefix } from "../utils/formatters";

type Props = {
  amount: number;
  currency: string;
  onChangeAmount: (value: number) => void;
} & TextFieldProps;

export function AmountInput({
  amount,
  onChangeAmount,
  currency,
  ...others
}: Props) {
  const inputComponent = React.useCallback(
    ({ inputRef, defaultValue, ...others }) => (
      <NumberFormat
        {...others}
        onValueChange={(value) => onChangeAmount(value.floatValue || 0)}
        getInputRef={inputRef}
        thousandSeparator
        isNumericString
        decimalScale={2}
        fixedDecimalScale
        prefix={currencyPrefix(currency)}
      />
    ),
    [currency]
  );

  return (
    <TextField
      InputProps={{ inputComponent }}
      {...others}
      variant="outlined"
      value={amount || ""}
    />
  );
}
