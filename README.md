# Currency Conversion using TypeScript Next.js

The app is hosted at https://currency-conversion.jcemer.now.sh.

It requires an https://www.alphavantage.co access key. Duplicate and rename the .env.local.example to .env.local and add your generated key.

Install it and run:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```
